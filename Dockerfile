FROM docker.io/library/debian:stable-20240926-slim

RUN apt-get -y update \
    && apt-get -y install --no-install-recommends jq bash perl yq git ca-certificates\
    && apt-get -y clean \
    && apt-get -y autoremove \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /var/cache/apt
